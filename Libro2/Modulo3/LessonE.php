  <link rel="stylesheet" type="text/css" href="..\..\bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="..\..\Estilos/estilos.css">
    <link rel="stylesheet" type="text/css" href="..\..\Estilos/EstilosIndex.css">
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <script async="async" src="Codigo/LessonE.js"></script>
<head>
    <title>Modulo 3: LessonE</title>
</head>    
<body bgcolor="#e3e7ed">
    <!--banner-->
     <!--Small screens header-->
     <div class="container-fluid hidden-sm-up" >
       <div class="row" align="center" >
          <div class="col-sm-12 col-xs-12 titulo "  align="left"><img align="left" src="..\..\Imagenes/manitas.png" height="40" width="70">
           <div class="titulo h4"><span class="colorLetra" style="margin-left: 20px">B</span>ritish <span class="colorLetra">C</span>ertification <span class="colorLetra">C</span>lub</div>
           </div>
     </div>
     </div>
        <!--+Medium screens header-->
   <div class="container-fluid hidden-xs-down " >
         <div class="row" >
             <div class="col-sm-12 col-md-7 col-lg-7  titulo h2" ><img align="left" src="..\..\Imagenes/manitas.png" height="45" width="80"><span class=" colorLetra" style=" margin-left: 15px;">B</span>ritish <span class="colorLetra">C</span>ertification <span class="colorLetra">C</span>lub</div>
             <div class="col-sm-5 col-md-5 col-lg-5 hidden-sm-down  " align="right"><img src="..\..\Imagenes/cambrige_logo.png" width="130px" height="80px"></div>
         </div>
    </div>
    <hr>
    <!--Bannner-->
    <div class="container-fluid col-xs-12 col-lg-12  hidden-sm-up" style="text-align: center;">
        <h3> Modulo 3 Lesson E</h3>    
    </div>
    <div class="container-fluid col-lg-12  hidden-xs-down" style="text-align: center;">
        <h1> Modulo 3: Lesson E</h1>    
    </div>
    
    <hr>
    <div class="container-fluid" >
        <div class="row" style="font-size: 18px">
            <div class="col-md-12 col-lg-7 "  align="center">
                <table id="puzzle"> </table>  
            </div>
            <div class="col-md-12 col-lg-4">
                <div id="hints_container">
                    <h3>Vertical</h3>
                    <div id="vertical_hints_container">
                    </div>
                    <h3>Horizontal</h3>
                    <div id="horizontal_hints_container">
                    </div>
                    <div id="buttons_container">
                        <button id="clear_all">Clear All</button>
                        <button id="check">Check</button>
                        <button id="solve">Solve</button> 
                        <button id="clue">Clue</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>


